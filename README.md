Tecnologia em Análise e Desenvolvimento de Sistemas
Setor de Educação Profissional e Tecnológica - SEPT
Universidade Federal do Paraná - UFPR
# Projeto Web1
## _Descrição do sistema funcionamento_
___
## Site
O trabalho conciste em um site de uma academia o qual permite o cadastro de clientes/ usuários. 
Além disso usuários podem se matricular em um dos planos oferecidos pela empresa.
_O  site possui sessões de login, mas não contam com cookies_

## Estrutura
___
### _Banco de dados_

O banco de dados é composto por três tabelas:
* Usuários
    * Id
    * Nome
    * Email
    * Password
* Planos
    * Id
    * Nome
    * Tarifa
* Matrícula
    * Id
    * Id_usuario
    * Id_plano
    * Data de Matrícula

_As tabelas de usuário e planos se relacionam com a tabela de matrícula_
### _Páginas_
As páginas são dividadas em home/index, login, cadastro e perfil de usuário.
Existem algumas páginas auxiliares principalmente para a criação do banco de dados e inserção de elementos na tabela de planos
no entanto essas estão disponíveis para usuários, e também, como o foco não é criar um sistema de gerenciamento para a empresa essas páginas não foram formatadas. 
 ___
## Funcionamento
### _Cadastro de usuários_
Conforme o banco de dados já descrito os dados armazenados são: nome, email e senha fornecidos pelo usuário. Ao realizar o registro é solicitado a confirmação da senha para evitar registro de senhas incorretas por erro de digitação.
Os filtros aplicado são: valida inputs de nome e email, confirma se email já não está cadastrado no banco de dados e por último confere se as senhas são iguais depois a converte para uma string _hash_ antes de armazenar no banco de dados.
### _Login_
O login como já mencionado funciona atravéz de sessões e não utiliza cookies, portanto, sempre que o usuário fechar o navegador terá que efetuar o login novamente.
Ao preencher e enviar o campo de email, o sistema confere se o email existe no banco de dados e retorna não só com verdadeiro ou falso, mas também, se verdadeiro com todos os dados do usuário encontrado. A partir de então confere o a senha. Se tudo estiver correto com o login então os dados do cliente são armazenados na sessão.
### _Logout_
Redicionamento do para a página de logout onde os dados da sessão são _unset_ e redireciona novamente para o index.
### _Perfil_
A página de perfil diponibilza a matrícula, se houver, e permite seu cancelamento. O sistema faz uma pesquisa no banco de dados indo as três tabelas onde o Id da sessão é equivalente ao Id do usuário no banco de dados e disponibiliza os dados da pesquisa para o usuário. Outra funcionalidade é, impedir o cliente de realizar mais de uma matrícula.
___