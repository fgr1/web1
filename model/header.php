<?php
session_start();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Project</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="http://localhost/web1/css/style.css" />

  </head>
  <body>
    <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light navbar fixed-top navbar-light bg-light">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Toggle button -->
    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarCenteredExample"
      aria-controls="navbarCenteredExample"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>

    <!-- Collapsible wrapper -->
    <div
      class="collapse navbar-collapse justify-content-center"
      id="navbarCenteredExample"
    >
      <!-- Left links -->
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <?php 
        
          if(isset($_SESSION["name"])) {
            echo "<li class='nav-item'><a class='nav-link' href='perfil.php'>$_SESSION[name]</a></li>";
            echo "<li class='nav-item'><a class='nav-link' href='http://localhost/web1/logout.php'>Logout</a></li>";
          }
          else {
            echo "<li class='nav-item'><a class='nav-link' href='login.php'>Login</a></li>";
            echo "<li class='nav-item'><a class='nav-link' href='cadastro.php'>Cadastrar</a></li>";
          }

        ?>
      </ul>
      <!-- Left links -->
    </div>
    <!-- Collapsible wrapper -->
  </div>
  <!-- Container wrapper -->
</nav>
    
    </header>
    <?php if(isset($_SESSION['msg'])): ?>
      <div class="justify-content-center text-center alert alert-<?= $_SESSION['tipo']; ?>" id="alerta">
        <?php  
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
            unset($_SESSION['tipo']);
        ?>
      </div>
    <?php endif ?>