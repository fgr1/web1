
<footer class="text-center text-lg-start text-muted">

  <section class="">
    <div class="container text-center text-md-start mt-5">

      <div class="row mt-3">

        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

          <h6 class="text-uppercase fw-bold mb-4">
            <i class="fas fa-gem me-3"></i>Nome da Empresa
          </h6>
        </div>

        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <h6 class="text-uppercase fw-bold mb-4">
            Planos
          </h6>
          <p>
            <a href="plano_Monstro.php" class="text-reset">Plano Monstro</a>
          </p>
          <p>
            <a href="plano_Frango.php" class="text-reset">Plano Frango</a>
          </p>
        </div>

        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <h6 class="text-uppercase fw-bold mb-4">
            Links
          </h6>
          <p>
            <a href="#!" class="text-reset">Suplementos</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Nutricionista</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Personal Trainners</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Ajuda</a>
          </p>
        </div>

        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

          <h6 class="text-uppercase fw-bold mb-4">
            Contato
          </h6>
          <p><i class="fas fa-home me-3"></i> Curitiba, PR, 81670-130, Brasil</p>
          <p>
            <i class="fas fa-envelope me-3"></i>
            email@institucional.com
          </p>
          <p><i class="fas fa-phone me-3"></i> +55 41 996898865</p>
          <p><i class="fas fa-print me-3"></i> +55 41 33761261</p>
        </div>

      </div>

    </div>
  </section>

  <div class="text-center p-4" id="rodape">
 
  </div>

</footer>



<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" 
    integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" 
    integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  </body>
</html>