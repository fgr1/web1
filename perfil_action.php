<?php
session_start();
require_once 'bd/credentials.php';
require_once 'function_action.php';

if (isset($_POST["frango"]) || isset($_POST["monstro"])){
    $usuario = $_SESSION["id"];
    $sql = "SELECT * FROM matricula WHERE id_user = '$usuario';";

    $lista = mysqli_query($conn, $sql);
    if (!$lista) {
        die("Error: " . $sql . "<br>" . mysqli_error($conn));
    }
    $row_cnt = mysqli_num_rows($lista);
    if($row_cnt != 0){
        $_SESSION['msg'] = "Já exite matrícula cadastrada";
        $_SESSION['tipo'] = "danger";
        header("Location: http://localhost/web1/index.php");
        exit;
    }

}

if(isset($_POST["frango"])) {
    $usuario = $_SESSION["id"];
    $today = date("Y-m-d");
    $plano = 2;
    
    $sql = "INSERT INTO matricula (id_user, id_plano, data_matricula) VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: http://localhost/web1/index.php?error=stmtfalhou");
        exit;
    }
    
    mysqli_stmt_bind_param($stmt, "iis", $usuario, $plano, $today);
    mysqli_stmt_execute($stmt);
    
    mysqli_stmt_close($stmt);
    header("Location: http://localhost/web1/perfil.php");
    exit;
}

if(isset($_POST["monstro"])) {
    $usuario = $_SESSION["id"];
    $today = date("Y-m-d");
    $plano = 1;

    $sql = "INSERT INTO matricula (id_user, id_plano, data_matricula) VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: http://localhost/web1/index.php?error=stmtfalhou");
        exit;
    }

    mysqli_stmt_bind_param($stmt, "iis", $usuario, $plano, $today);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("Location: http://localhost/web1/perfil.php");
    exit;
}

if(isset($_POST["cancelaMatricula"])) {
    $id_matricula = $_POST["id_matricula"];
    $sql = "DELETE FROM matricula WHERE id = '$id_matricula';";

    $cancela = mysqli_query($conn, $sql);
        if (!$cancela) {
            die("Error: " . $sql . "<br>" . mysqli_error($conn));
        }
        
    header("Location: http://localhost/web1/perfil.php");
    exit;
}