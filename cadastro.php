<?php include 'model/header.php'; ?>
    <body>
    <section class="vh-100" id="section_cadastro">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <h3 class="mb-5">Cadastro</h3>

            <form action="cadastro_action.php" method="post">
              <div class="form-outline mb-4">
                <input type="text" id="typeEmailX-2" class="form-control form-control-lg" placeholder="Nome" name="name" />
              </div>

              <div class="form-outline mb-4">
                <input type="email" id="typeEmailX-2" class="form-control form-control-lg" placeholder="Email" name="email" />
              </div>

              <div class="form-outline mb-4">
                <input type="password" id="typePasswordX-2" class="form-control form-control-lg" placeholder="Senha" name="password"/>
              </div>

              <div class="form-outline mb-4">
                <input type="password" id="typePasswordX-2" class="form-control form-control-lg" placeholder="Digite a senha novamente" name="repassword"/>
              </div>

              <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">Cadastrar</button>
            </form>

            <hr class="my-4">

            <div>
              <p class="mb-0">Já tem cadastro? <a href="login.php" class="text-blue-50 fw-bold">Login</a></p>
            </div>
            <div id="alerta">
              <?php 
                if(isset($_GET["error"])) {

                  if($_GET["error"] == "inputvazio") {
                    echo "<p>Preencha todos os campos</p>";
                  } 
                  else if ($_GET["error"] == "nomeinvalido") {
                    echo "<p>Nome inválido</p>";
                  } 
                  else if ($_GET["error"] == "emailinvalido") {
                    echo "<p>Email inválido</p>";
                  } 
                  else if ($_GET["error"] == "passworddiff") {
                    echo "<p>Senhas não conferem</p>";
                  } 
                  else if ($_GET["error"] == "emailusado") {
                    echo "<p>Email já cadastrado</p>";
                  } 
                  else if ($_GET["error"] == "stmtfalhou") {
                    echo "<p>Eita aí é duro hein Reginaldo!</p>";
                  } 
                  else if ($_GET["error"] == "none") {
                    echo "<p>Cadastro concluído com sucesso!</p>";
                  }
                } 
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'model/footer.php'; ?>