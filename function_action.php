<?php

function inputVazioCadastro($name, $email, $password, $repassword) {
    $result;
    if(empty($name) || empty($email) || empty($password) || empty($repassword)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function validarNome($nome) {
    $result;
    if(!filter_var($nome, FILTER_SANITIZE_SPECIAL_CHARS)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function validarEmail( $email) {
    $result;
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function passwordConfere($password, $repassword) {
    $result;
    if($password !== $repassword) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function emailExist($conn, $email) {
    $result;
    $sql = "SELECT * FROM users WHERE email = ?;";
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: http://localhost/web1/cadastro.php?error=stmtfalhou");
        exit;
    }

    mysqli_stmt_bind_param($stmt, "s", $email);
    mysqli_stmt_execute($stmt);

    $resultadoDado = mysqli_stmt_get_result($stmt);

    if($dados = mysqli_fetch_assoc($resultadoDado)) {
        return $dados; 
    } else {
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}

function createUser($conn, $name, $email, $password) {
    $result;
    $sql = "INSERT INTO users (name, email, senha) VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: http://localhost/web1/cadastro.php?error=stmtfalhou");
        exit;
    }

    $hash = password_hash($password, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "sss", $name, $email, $hash);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("Location: http://localhost/web1/cadastro.php?error=none");
    exit;
}

function inputVazioLogin($email, $password) {
    $result;
    if(empty($email) || empty($password)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function loginUser($conn, $email, $password) {
  $usuario =  emailExist($conn, $email);

  if($usuario === false) {
    header("Location: http://localhost/web1/login.php?error=loginfalhou");
    exit;
  }

  $pwdhash = $usuario['senha'];
  $checkpwd = password_verify($password, $pwdhash);

  if($checkpwd === false) {
    header("Location: http://localhost/web1/login.php?error=loginfalhou");
    exit;
  }
  else if ($checkpwd === true) {
    session_start();
    $_SESSION["id"] = $usuario["id"];
    $_SESSION["name"] = $usuario["name"];
    $_SESSION["email"] = $usuario["email"];
    $_SESSION["plano"] = $usuario["plano"];
    header("Location: http://localhost/web1/perfil.php");
    exit;
  }
}