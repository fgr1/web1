<?php
session_start();
require_once 'bd/credentials.php';
require_once 'function_action.php';

if(!isset($_POST["submit"])) {
    header("Location: ".$base."/index.php");
    exit;
}

if(!isset($_SESSION["id"])) {
    header("Location: ".$base."/login.php");
    exit;
}

$usuario = $_SESSION["id"];
$today = getdate();
$plano = 2;

    $sql = "INSERT INTO matricula (id_user, id_plano, data_matricula) VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: http://localhost/web1/index.php?error=stmtfalhou");
        exit;
    }

    mysqli_stmt_bind_param($stmt, "iis", $usuario, $plano, $today);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("Location: http://localhost/web1/perfil.php");
    exit;