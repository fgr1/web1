<?php 
    include 'model/header.php';
    session_start();
    require_once 'bd/credentials.php';
?>
<?php
    $usuario = $_SESSION["id"];

    $sql = "SELECT users.name AS usuario, planos.name, planos.tarifa, matricula.id AS matricula
                FROM matricula 
                INNER JOIN users ON matricula.id_user = users.id
                INNER JOIN planos ON matricula.id_plano = planos.id
                WHERE users.id =  $usuario;";

    $result = mysqli_query($conn, $sql);
        if (!$result) {
            die("Error: " . $sql . "<br>" . mysqli_error($conn));
        }                  
?>
<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 text-center bg-light" id="capa_perfil">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">Seja bem vindo <?= $_SESSION["name"]; ?></h1>
        </div>
    </div>
    
    <div class="container" id="corpo_perfil">

        <div class="p-3 pb-md-4 mx-auto text-center">
            <h1 class="display-4 fw-normal">Planos</h1>
            <p class="fs-5 text-muted">Matrículas ativas</p>
        </div>
        <?php if (mysqli_num_rows($result) > 0): ?>
        <?php while($plano = mysqli_fetch_assoc($result)): ?>
        <div class="row row-cols-1 row-cols-md-1 mb-1 text-center">
            <form action="perfil_action.php" method="post">
                <div class="col">
                    <div class="card mb-4 rounded-3 shadow-sm">
                        <div class="card-header py-3 text-white" id="cart_princ">
                            <h4 class="my-0 fw-normal"><?= $plano["name"]; ?></h4>
                        </div>
                        <div class="card-body">
                            <h1 class="card-title"><?= $plano["tarifa"]; ?><small class="text-muted fw-light">/mês</small></h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li>Acesso ilimitado a todas áreas da academia</li>
                                <li>Aulas de Ginástica</li>
                                <li>Área de musculação e aeróbicos</li>
                                <?php if($plano["name"] == 'Plano Frango'): ?>
                                <li>Sem taxa de cancelamento</li>
                                <li>- </li>
                                <li>- </li>
                                <?php endif ?>
                                <?php if($plano["name"] == 'Plano Monstro'): ?>
                                <li>Leve amigos para treinar com você</li>
                                <li>1 mês para presentear alguém</li>
                                <li>Ganhe Kit camiseta + garrafa d'água</li>
                                <?php endif ?>
                            </ul>
                            <input type="hidden" name="id_matricula" value="<?= $plano["matricula"]; ?>">
                            <button  type="submit" name="cancelaMatricula" class="w-100 btn btn-lg btn-danger">Cancelar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</main>
<?php include 'model/footer.php'; ?>