<?php include 'model/header.php'; ?>

<main>

    <div class="position-relative overflow-hidden p-3 p-md-5 text-center bg-light" id="capa">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">Academia</h1>
            <p class="lead fw-normal">Seja mais forte que as suas desculpinhas! Venha treinar conosco!  </p>
            <?php if(!isset($_SESSION["id"])): ?>
            <a class="btn btn-outline-secondary" href="cadastro.php">Cadastre-se</a>
            <?php endif ?>
        </div>
    </div>

    <div class="container py-2">

        <div class="p-3 pb-md-4 mx-auto text-center">
            <h1 class="display-4 fw-normal">Planos</h1>
            <p class="fs-5 text-muted">Treine conosco! Ambos os planos oferencem área de musculação, aerobico e aulas especiais. </p>
        </div>

    
        <div class="row row-cols-1 row-cols-md-1 mb-1 text-center">
            <form action="<?php if(isset($_SESSION["id"])) {
                echo "perfil_action.php";
            } else {
                echo "login.php";
            } ?>" method="post" class="row row-cols-2 row-cols-md-2 mb-2 text-center">
                <div class="col">
                    <div class="card mb-4 rounded-3 shadow-sm">
                        <div class="card-header py-3">
                            <h4 class="my-0 fw-normal">Plano Frango</h4>
                        </div>
                        <div class="card-body">
                            <h1 class="card-title">R$80,00<small class="text-muted fw-light">/mês</small></h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li>Acesso ilimitado a todas áreas da academia</li>
                                <li>Aulas de Ginástica</li>
                                <li>Área de musculação e aeróbicos</li>
                                <li>Sem taxa de cancelamento</li>
                                <li>- </li>
                                <li>- </li>
                            </ul>
                            <button  type="submit" name="frango" class="w-100 btn btn-lg btn-primary">Registrar</button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card mb-4 rounded-3 shadow-sm" >
                        <div class="card-header py-3 text-white" id="cart_princ">
                            <h4 class="my-0 fw-normal">Plano Monstro</h4>
                        </div>
                        <div class="card-body">
                            <h1 class="card-title">R$100,00<small class="text-muted fw-light">/mês</small></h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li>Acesso ilimitado a todas áreas da academia</li>
                                <li>Aulas de Ginástica</li>
                                <li>Área de musculação e aeróbicos</li>
                                <li>Leve amigos para treinar com você</li>
                                <li>1 mês para presentear alguém</li>
                                <li>Ganhe Kit camiseta + garrafa d'água</li>
                            </ul>
                            <button  type="submit" name="monstro" class="w-100 btn btn-lg btn-primary">Registrar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>




<?php include 'model/footer.php'; ?>