<?php include 'model/header.php'; ?>
    <body>
    <section class="vh-100" id="section_login">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <h3 class="mb-5">Sign in</h3>
            <form action="login_action.php" method="post">  
              <div class="form-outline mb-4">
                <input type="email" id="typeEmailX-2" class="form-control form-control-lg" placeholder="Email" name="email" />
              </div>

              <div class="form-outline mb-4">
                <input type="password" id="typePasswordX-2" class="form-control form-control-lg" placeholder="Senha" name="password"/>
              </div>

              <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">Login</button>
            </form>
            <hr class="my-4">

            <div>
              <p class="mb-0">Não tem cadastro? <a href="cadastro.php" class="text-blue-50 fw-bold">Cadastrar</a>
              </p>
            </div>
            <div id="alerta">
              <?php 
                if(isset($_GET["error"])) {

                  if($_GET["error"] == "inputvazio") {
                    echo "<p>Preencha todos os campos</p>";
                  } 
                  else if ($_GET["error"] == "loginfalhou") {
                    echo "<p>Email ou senha incorretos</p>";
                  } 
                } 
              ?>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include 'model/footer.php'; ?>