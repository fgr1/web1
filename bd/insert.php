<?php
    include "credentials.php";

    $sql = "SELECT * FROM planos";
    $rows = mysqli_query($conn,$sql);

    if(!$rows)
        die("Erro sql: " . mysqli_error($conn));

    mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insere Planos</title>
</head>
<body>
    <form action="insert_action.php" method="POST">
        Name: <input type="text" name="name"><br>
        Tarifa: <input type="text" name="tarifa"><br>
        <input type="submit" value="Enviar">
    </form>
    <?php

        if(mysqli_num_rows($rows) > 0){
            while($planos = mysqli_fetch_assoc($rows)){
                echo $planos["name"] . " " . $planos["tarifa"] . "<br>";
            }
        }
        else{
            echo "Nenhum registro na tabela";
        }

    ?>
</body>
</html>