<?php
    include "credentials.php";

    $sql = "create database web1;";

    if (mysqli_query($conn, $sql)) {
        echo "DataBase criada com sucesso!";
    } else {
        echo "Erro: " . mysqli_error($conn);
    }

    $sql = "use web1;";

    if (mysqli_query($conn, $sql)) {
        echo "ok!";
    } else {
        echo "Erro: " . mysqli_error($conn);
    }
    // sql to create table planos
    $sql = "CREATE TABLE planos (
    id INT(10) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    tarifa DECIMAL(4, 2)
    )";
    
    if (mysqli_query($conn, $sql)) {
        echo "Table planos ok!";
    } else {
        echo "Erro: " . mysqli_error($conn);
    }

    // sql to create table users
    $sql = "CREATE TABLE users (
    id INT(10) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100),
    senha VARCHAR(200)
    )";

    if (mysqli_query($conn, $sql)) {
        echo "Table users ok!";
    } else {
        echo "Erro: " . mysqli_error($conn);
    }

    // sql to create table users
    $sql = "CREATE TABLE matricula (
    id INT(10) AUTO_INCREMENT PRIMARY KEY,
    id_user INT(10),
    id_plano INT(10),
    data_matricula DATE,
    CONSTRAINT fk_userma FOREIGN KEY (id_user) REFERENCES users (id),
    CONSTRAINT fk_planoma FOREIGN KEY (id_plano) REFERENCES planos (id)
    )";

    if (mysqli_query($conn, $sql)) {
        echo "Table matricula ok!";
    } else {
        echo "Erro: " . mysqli_error($conn);
    }
    mysqli_close($conn);