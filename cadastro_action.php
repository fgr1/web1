<?php

require_once 'bd/credentials.php';
require_once 'function_action.php';

if(!isset($_POST["submit"])) {
    header("Location: ".$base."/cadastro.php");
    exit;
}

$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
$repassword = $_POST['repassword'];


if(inputVazioCadastro($name, $email, $password, $repassword) !== false) {
    header("Location: ".$base."/cadastro.php?error=inputvazio");
    exit;
}

if(validarNome($name) !== false) {
    header("Location: ".$base."/cadastro.php?error=nomeinvalido");
    exit;
}

if(validarEmail($email) !== false) {
    header("Location: ".$base."/cadastro.php?error=emailinvalido");
    exit;
}

if(passwordConfere($password, $repassword) !== false) {
    header("Location: ".$base."/cadastro.php?error=passworddiff");
    exit;
}

if(emailExist($conn, $email) !== false) {
    header("Location: ".$base."/cadastro.php?error=emailusado");
    exit;
}

createUser($conn, $name, $email, $password);