<?php

require_once 'bd/credentials.php';

if(!isset($_POST["submit"])) {
    header("Location: ".$base."/login.php");
    exit;
}

$email = $_POST['email'];
$password = $_POST['password'];

require_once 'function_action.php';

if(inputVazioLogin($email, $password) !== false) {
    header("Location: ".$base."/login.php?error=inputvazio");
    exit;
}

if(loginUser($conn, $email, $password)) {

}
else {
    header("Location: ".$base."/login.php");
    exit;
}
